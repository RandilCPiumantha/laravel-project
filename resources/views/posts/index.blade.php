@extends('layouts.app') 

@section('content')
        <h1>Posts</h1>
        @if(count($posts)> 0)
        <div class="card">
            <ul class="list group list-group-flush">
            @foreach($posts as $post)
                <div class="class row">
                    <div class="class col-md-4">
                        <img style="width:100% " src="/storage/cover_images/{{$post->cover_image}}" alt =""/>
                    </div>
                    <div class="class col-md-8">
                        <h1><a href="/posts/{{$post->id}}">{{$post->title}}</a></h1>
                        <small>Written On {{$post->created_at}}</small>
                    </div>
                </div>                    
            @endforeach
            </ul>
        </div>
        @else
        @endif
@endsection